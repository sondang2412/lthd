// Require.js allows us to configure shortcut alias
// Their usage will become more apparent futher along in the tutorial.
require.config({
  paths: {
    // Major libraries
    jquery: 'libs/jquery/jquery-min',
    jqueryCookie: 'libs/jquery/jquery.cookie',
    jqueryIcheck: 'libs/jquery/jquery.icheck',
    jquerySerialize: 'libs/jquery/jquery.serialize',
    holder: 'holder',
    underscore: 'libs/underscore/underscore-min', // https://github.com/amdjs
    lodash: 'libs/lodash/lodash', // alternative to underscore
    backbone: 'libs/backbone/backbone-min', // https://github.com/amdjs
    sinon: 'libs/sinon/sinon.js',
    bootstrap: 'libs/bootstrap/bootstrap',
    bootstrapDialog: 'libs/bootstrap/bootstrap-dialog',
    bootstrapPaginator: 'libs/bootstrap/bootstrap-paginator',
    fileinput: 'libs/bootstrap/fileinput',
    gmap:'libs/google/googlemap',
    async: 'libs/require/async',
    text: 'libs/require/text',
    myjs: 'libs/myjs/myjs',

    // Just a short cut so we can put our html outside the js dir
    // When you have HTML/CSS designers this aids in keeping them out of the js directory
    templates: '../templates',

    facebook: 'libs/require/fb'
  },

  shim:{
    jqueryCookie:{
      deps:['jquery']
    },
    jqueryIcheck:{
      deps:['jquery']
    },
    jquerySerialize:{
      deps:['jquery']
    },
    facebook: {
      exports: "FB"
    },
    bootstrap:{
      deps:['jquery']
    },
    bootstrapDialog: {
      deps:['bootstrap'],
      exports: 'BootstrapDialog'
    },
    bootstrapPaginator: {
      deps:['bootstrap']
    },
    fileinput:{
      deps:['bootstrap', 'holder']
    }

  }

});

define('gmaps', ['async!http://maps.googleapis.com/maps/api/js?key=xxx&sensor=false'], function() {
    return google.maps;
});

// Let's kick off the application
require([
  'views/app',
  'router',
  'vm', 'facebook'
], function(AppView, Router, Vm){
  var appView = Vm.create({}, 'AppView', AppView);
  appView.render();
  Router.initialize({appView: appView});  // The router now has a copy of all main appview
});
