function setUrl(url, name, value){
	var newAdditionalURL = "";
	var tempArray = url.split("?");
	var baseURL = tempArray[0];
	var aditionalURL = tempArray[1];
	var temp = "";
	if (aditionalURL)
	{
		var tempArray = aditionalURL.split("&");
		for (var i in tempArray) {
			if (tempArray[i].indexOf(name) === -1) {
				newAdditionalURL += temp + tempArray[i];
				temp = "&";
			}
		}
	}
	var rows_txt = temp + name + "=" + value;
	var finalURL = baseURL + "?" + newAdditionalURL + rows_txt;
	return finalURL;
};

function getURLParameter(name) {
return decodeURI(
  (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
);
}