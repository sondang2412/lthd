// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'vm'
], function ($, _, Backbone, Vm) {
  var AppRouter = Backbone.Router.extend({
    routes: {
      // Pages
      'modules': 'modules',
      'optimize': 'optimize',
      'backbone/:section': 'backbone',
      'backbone': 'backbone',
      'manager': 'manager',       
      'admin' : 'admin',
      'adminManagerUser' : 'adminManagerUser',
      'article/add': 'addArticle',
      'detail/:id': 'detail',
      // Default - catch all
      '*actions': 'home',
      'search': 'search'

    }
  });

  var initialize = function(options){
    var appView = options.appView;
    var router = new AppRouter(options);

    // admin
    router.on('route:admin', function () {
      require(['views/admin/admin'], function (AdminPage) {
        var optimizePage = Vm.create(appView, 'Admin', AdminPage);
        optimizePage.render();
      });
    });
    router.on('route:adminManagerUser', function () {
      require(['views/admin/adminManagerUser'], function (adminManagerUser) {
        var optimizePage = Vm.create(appView, 'Admin', adminManagerUser);
        optimizePage.render();
      });
    });
    // end admin

    router.on('route:addArticle', function () {
      require(['views/article/add'], function (AddArticlePage) {
        var optimizePage = Vm.create(appView, 'AddArticle', AddArticlePage);
            optimizePage.render();
      });
    });

    router.on('route:detail', function (id) {
      require(['views/detail/detail'], function (DetailPage) {
        var optimizePage = Vm.create(appView, 'DetailPage', DetailPage, {id:id});
        optimizePage.render(id);
      });
    });

    router.on('route:search', function () {
      require(['views/search/search'], function (SearchPage) {
        var optimizePage = Vm.create(appView, 'SearchPage', SearchPage);
        optimizePage.render();
      });
    });

    router.on('route:home', function () {
      require(['views/home/home'], function (HomePage) {
        var optimizePage = Vm.create(appView, 'HomePage', HomePage);
        optimizePage.render();
      });
    });

    router.on('route:optimize', function () {
      require(['views/optimize/page'], function (OptimizePage) {
        var optimizePage = Vm.create(appView, 'OptimizePage', OptimizePage);
        optimizePage.render();
      });
    });
    router.on('route:defaultAction', function (actions) {
      require(['views/dashboard/page'], function (DashboardPage) {
        var dashboardPage = Vm.create(appView, 'DashboardPage', DashboardPage);
        dashboardPage.render();
      });
    });
    router.on('route:modules', function () {
     require(['views/modules/page'], function (ModulePage) {
        var modulePage = Vm.create(appView, 'ModulesPage', ModulePage);
        modulePage.render();
      });
    });
    router.on('route:backbone', function (section) {
      require(['views/backbone/page'], function (BackbonePage) {
        var backbonePage = Vm.create(appView, 'BackbonePage', BackbonePage, {section: section});
        backbonePage.render();
      });
    });
    router.on('route:manager', function () {
      require(['views/manager/page'], function (ManagerPage) {
        var managerPage = Vm.create(appView, 'ManagerPage', ManagerPage);
        managerPage.render();
      });
    });
    Backbone.history.start();
  };
  return {
    initialize: initialize
  };
});
