define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/header/header.html',
  'facebook',
  'bootstrapDialog'
], function($, _, Backbone, headerMenuTemplate){
  var HeaderMenuView = Backbone.View.extend({
    el: '#header',
    initialize: function () {
    },
    render: function () {

      $(this.el).html(headerMenuTemplate);
      FB.init({
          appId      : '238667939622562',
          status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true  // parse XFBML
        }); 
      var that = this;
      require(['views/user/userControl', 'vm'], function (UserControlView, Vm) {
        var userControlView = Vm.create(that, 'UserControlView', UserControlView);
        userControlView.render();
      })
    },
    events: {
      "click #home-page-link": "goHomePage",
      'click #create-article-link' : "goCreateArticle",
      'click #manage-article-link' : "goManageArticle",
      'click #admin-article-link' : "goAdminArticle",
      'click #admin-user-link' : "goAdminUser",
      'click #admin-parse-article' : "confirmParse"
    },
    goHomePage: function(ev){
      require(['views/home/home', 'vm', 'views/app'], function (HomeView, Vm, appView) {
        var homeView = Vm.create(appView, 'HomeView', HomeView);
        homeView.render(1, "", false, function(){
          var menuList = $("#menu-list").find("li");
          $(menuList).each(function(index){
            if ($(menuList[index]).hasClass("active")){
              $(menuList[index]).removeClass("active");
            }
          });
          $(ev.currentTarget).parent("li").addClass("active");
        });
      });
      return false;
    },
    goCreateArticle: function(ev){
      require(['views/article/add', 'vm', 'views/app'], function (CreateArticleView, Vm, appView) {
        var createArticleView = Vm.create(appView, 'CreateArticleView', CreateArticleView);
        createArticleView.render();
        var menuList = $("#menu-list").find("li");
        $(menuList).each(function(index){
            if ($(menuList[index]).hasClass("active")){
              $(menuList[index]).removeClass("active");
            }
          });
          $(ev.currentTarget).parent("li").addClass("active");
      });
      return false;
    },
    goManageArticle: function(ev){
      require(['views/home/home', 'vm', 'views/app'], function (HomeView, Vm, appView) {
        var homeView = Vm.create(appView, 'HomeView', HomeView);
        homeView.render(1, "", true, function(){
          var menuList = $("#menu-list").find("li");
          $(menuList).each(function(index){
            if ($(menuList[index]).hasClass("active")){
              $(menuList[index]).removeClass("active");
            }
          });
          $(ev.currentTarget).parent("li").addClass("active");
        });
      });
      return false;
    },
    goAdminArticle: function(){
      require(['views/admin/adminArticle', 'vm', 'views/app'], function (AdminArticleView, Vm, appView) {
        var adminArticleView = Vm.create(appView, 'AdminArticleView', AdminArticleView);
        adminArticleView.render();
      })
    },
    goAdminUser: function(){
      require(['views/admin/adminManagerUser', 'vm', 'views/app'], function (AdminUserView, Vm, appView) {
        var adminUserView = Vm.create(appView, 'AdminArticleView', AdminUserView);
        adminUserView.render();
      })
    },
    confirmParse: function(){
      var dialogGlobalRef;
      BootstrapDialog.show({
          message: "Rút trích dữ liệu từ web khác sẽ mất vài phút, bạn có chắc không?",
          buttons: [{
              label: 'Yes',
              cssClass: 'btn-primary',
              autospin: true,
              action: function(dialogRef) {
                  dialogRef.enableButtons(false);
                  dialogRef.setClosable(false);
                  dialogGlobalRef = dialogRef;
                  parseArticle();
              }
          }, {
              label: 'No',
              action: function(dialogRef) {
                  dialogRef.close();
              }
          }]
      });   

      function parseArticle(){
        var ParseModel = Backbone.Model.extend({url:"ExtractInfo"});
        var parseModel = new ParseModel();
        parseModel.save(null, {
          success: function(){
            dialogGlobalRef.close();
            BootstrapDialog.show({
            title: "Success",
            message: "Rút trích dữ liệu thành công",
            buttons: [{
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
              }]
            });
          },
          error: function(){
            dialogGlobalRef.close();
            BootstrapDialog.show({
            title: "Error",
            message: "Có lỗi xảy ra, rút trích thất bại",
            buttons: [{
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
              }]
            });
          }
        });
      }
    }

  })

  return HeaderMenuView;
});
