define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/search/search.html'
], function($, _, Backbone, searchMenuTemplate){
  var HeaderMenuView = Backbone.View.extend({
    el: '#page-content',
    initialize: function () {
    },
    render: function () {

      $(this.el).html(searchMenuTemplate);
    }
  })

  return HeaderMenuView;
});
