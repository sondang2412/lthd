define([
  'gmap',
  'jquery',
  'lodash',
  'backbone',
  'text!templates/detail/detail.html',
  'facebook'
], function(googleMap, $, _, Backbone, detailMenuTemplate){
  var HeaderMenuView = Backbone.View.extend({
    el: '#page-content',
    initialize: function () {
    },
    render: function (id) {
      var HouseModel = Backbone.Model.extend({
        url: "house",
        id: id
      });
      var houseModel = new HouseModel();

      var that = this;
      houseModel.fetch({
        data: $.param({ Id: id}),
        success: function(house){
          var template = _.template(detailMenuTemplate, {house: house});
          $(that.el).html(template);

          // render left side bar
          require(['views/leftbar/leftbar', 'vm'], function (LeftSideView, Vm) {
            var leftSideView = Vm.create(that, 'LeftSide', LeftSideView);
            leftSideView.render();
          }); 
        }
      }); 
    }
  })

  return HeaderMenuView;
});
