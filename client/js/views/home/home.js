define([
  'views/app',
  'views/detail/detail',
  'jquery',
  'lodash',
  'backbone',
  'text!templates/home/home.html',
  'vm',
  'bootstrapPaginator',
  'myjs'
], function(appView, detailView, $, _, Backbone, homeMenuTemplate, Vm){
  var HeaderMenuView = Backbone.View.extend({
    el: '#page-content',
    initialize: function () {
    },
    render: function (curPage, districtID, forPoster, successRender) {

      if (curPage == null)
        curPage = 1;
      var that = this;

      if (districtID == null)
        districtID = "";

      if (forPoster == null)
        forPoster = false;

      var HouseModel = Backbone.Model.extend({
        url: "house"
      });

      var houseModel = new HouseModel();
      houseModel.fetch({
        data: $.param({ CurPage: curPage, DistrictID: districtID, ForPoster: forPoster}),
        success: function(houses){
            var template = _.template(homeMenuTemplate, {houses: houses});
              that.$el.html(template);
              if (successRender != null)
                successRender();

              if (houses.get("TotalPage") == 0)
                houses.set("TotalPage", 1);
              
              var paginationOptions = {
                  currentPage: curPage,
                  totalPages: houses.get("TotalPage"),
                  pageUrl: function(type, page, current) {
                    return page;
                  }
              };

              $('#pagination').bootstrapPaginator(paginationOptions);
              $('.pagination-item').click(function(){
                that.render($(this).attr('href'), districtID, forPoster);
                return false;
              });

              // render left side bar
              require(['views/leftbar/leftbar'], function (LeftSideView) {
                var leftSideView = Vm.create(that, 'LeftSide', LeftSideView);
                leftSideView.render(forPoster);
              }); 

              if (forPoster == true){
                  $.each($(".box-edit"), function(index){
                    $(".box-edit").eq(index).html('<a href="#" class="btn btn-primary edit-article-link"><div class="glyphicon glyphicon-edit"></div>&nbsp;Chỉnh sửa</a>');
                  })
              }
          
        } // end  fecth houses success
      }); // end fetch house
    }, // end render
    events:{
      'click .detail-link' : 'viewDetail',
      'click .edit-article-link' : 'editArticle'
    },
    viewDetail: function(ev){
      var id = $(ev.currentTarget).attr("id");
      var optimizePage = Vm.create(appView, 'DetailPage', detailView, {id: id});
      optimizePage.render(id);
      ev.preventDefault();
    },
    editArticle: function(ev){
      ev.preventDefault();
      var id = $(ev.currentTarget).parent("div").eq(0).attr("id");
      require(['views/article/add', 'views/app'], function (EditArticleView, appView) {
        var editArticleView = Vm.create(appView, 'EditArticleView', EditArticleView);
        editArticleView.render(id);
      });
    }
  })

  return HeaderMenuView;
});
