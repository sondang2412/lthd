define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/article/addArticle.html',
  'gmap',
  'bootstrapDialog',
  'jqueryIcheck',
  'fileinput',
  'jquerySerialize'
], function($, _, Backbone, addArticleTemplate, googleMap){
  var HeaderMenuView = Backbone.View.extend({
    el: '#page-content',
    initialize: function () {
    },
    render: function (id) {
      var that =  this;
      var DistrictModel = Backbone.Collection.extend({
        url: "district"
      });
      var districtList = new DistrictModel();
      districtList.fetch({
        success: function(districts){

          var HouseModel = Backbone.Model.extend({
            url: "house"
          });
          var houseModel = new HouseModel();
          if (id != null){
            houseModel.fetch({
              data: $.param({Id : id}),
              success: function(house){
                console.log(house);
                var template = _.template(addArticleTemplate, {house: house, districts: districts.models});
                $(that.el).html(template);
              }
            });
          }
          else{
            var template = _.template(addArticleTemplate, {house: new HouseModel(), districts: districts.models});
            $(that.el).html(template);
          }
        }
      });

    },
    events:{
      'submit form' : 'confirm'
    },
    confirm : function(ev){
      ev.preventDefault();
      var dialogGlobalRef;
      var messageGlobal = "abc";
      BootstrapDialog.show({
          message: "Bạn có chắc chắn thông tin đã nhập không?",
          buttons: [{
              label: 'Yes',
              cssClass: 'btn-primary',
              autospin: true,
              action: function(dialogRef) {
                  dialogRef.enableButtons(false);
                  dialogRef.setClosable(false);
                  dialogGlobalRef = dialogRef;
                  addArticle();
              }
          }, {
              label: 'No',
              action: function(dialogRef) {
                  dialogRef.close();
              }
          }]
      });   

      function addArticle(){
        var data = new FormData();
        var i = 0;
        $(".image-input").each(function(){
          var files = $(this).get(0).files;
          if (files.length > 0){
            data.append("file" + i, files[0]);
          }
          i++;
        });
        var article = $(ev.currentTarget).serializeObject();
        $.ajax({
          type: "POST",
          url: "upload",
          contentType: false,
          processData: false,
          data: data,
          success: function(response){
            console.log("SUCCESS");
            console.log(response);
            var article = $(ev.currentTarget).serializeObject();
            console.log(article);
            var HouseModel = Backbone.Model.extend({
              url: "/house"
            });
            var house = new HouseModel();
            article.ImgURL = "";
            $(response).each(function(index){
              article.ImgURL += response[index] + ";";
            });
            $.each($("img.image-display"), function(index){
              article.ImgURL += $("img.image-display").eq(index).attr("src") + ";";
            });
            house.save(article, {
              success: function(){
                dialogGlobalRef.close();
                BootstrapDialog.show({
                title: "Success",
                message: "Đăng bài thành công",
                buttons: [{
                    label: 'Close',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                  }]
                });
              },
              error: function(){
                dialogGlobalRef.close();
                BootstrapDialog.show({
                title: "Lỗi",
                message: "Có lỗi xảy ra, vui lòng xem lại thông tin",
                buttons: [{
                    label: 'Close',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                  }]
                });
              }
            });
          },
          error: function(error){
            console.log("ERROR");
            console.log(error);
            dialogGlobalRef.close();
            BootstrapDialog.show({
            title: "Lỗi",
            message: "Có lỗi xảy ra, vui lòng xem lại thông tin",
            buttons: [{
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
              }]
            });
          }
        }); // end ajax POST

      } // end addArticle function

    } // end confirm function

  });

  return HeaderMenuView;
});
