define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/profile/edit.html',
  'jquerySerialize',
  'bootstrapDialog'
], function($, _, Backbone, dashboardPageTemplate){
  var DashboardPage = Backbone.View.extend({
    el: '#page-content',
    render: function () {
      var UserModel = Backbone.Model.extend({
        url: "user"
      });
      var that = this;
      var userModel = new UserModel();
      userModel.fetch({
        success: function(response){
          console.log(response);
          var templates = _.template(dashboardPageTemplate, {user: response});
          $(that.el).html(templates);
        }
      });
    },
    events:{
      "submit #edit-profile-form" : "editProfile"
    },
    editProfile : function(ev){
      ev.preventDefault();
      var profile = $(ev.currentTarget).serializeObject();
      var UserModel = Backbone.Model.extend({
        url: "user"
      });
      var userModel = new UserModel();
      userModel.save(profile, {
        success: function(){
          // update user control
          require(['views/user/userControl', 'vm', 'views/header/header'], function (UserControlView, Vm, HeaderView) {
            var userControlView = Vm.create(HeaderView, 'UserControlView', UserControlView);
            userControlView.render();
          });
          BootstrapDialog.show({
            message: "Thay đổi thông tin cá nhân thành công",
            buttons: [{
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
              }]
          });
        }
      });
    }
  });
  return DashboardPage;
});
