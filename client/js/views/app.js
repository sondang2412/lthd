define([
  'jquery',
  'lodash',
  'backbone',
  'vm',
  'events',
  'text!templates/layout.html',
  'facebook',
  'jqueryCookie',
  'myjs',
  'bootstrapDialog'
], function($, _, Backbone, Vm, Events, layoutTemplate, FB){
  var AppView = Backbone.View.extend({
    el: '#content',
    initialize: function(){

      // update user control
      require(['views/user/userControl', 'vm', 'views/header/header'], function (UserControlView, Vm, HeaderView) {
        var userControlView = Vm.create(HeaderView, 'UserControlView', UserControlView);
        userControlView.render();
      });
      
      var that = this;
      // set ajax prefix
      $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
          if (originalOptions.dataType === "script"){

          }
          else{
            if ($.cookie("oauth_token"))
              options.url = setUrl("http://nhatro.apphb.com/api/" + options.url, "oauth_token", $.cookie("oauth_token"));
            //  options.url = "http://localhost:38940/api/" + options.url + "?&oauth_token=" + $.cookie("oauth_token");
            else
              options.url = setUrl("http://nhatro.apphb.com/api/" + options.url, "oauth_token", "");
          }
      });

      // handle facebook change
      FB.Event.subscribe('auth.statusChange', function(response) {
          // update login button
          var temp = $(".fb-login-button").find("iframe");
          $(temp).attr('src', $(temp).attr('src'));

          if (response.status === 'connected') {
            var uid = response.authResponse.userID;
            accessToken = response.authResponse.accessToken;
            console.log("Facebook access token: " + accessToken);
            // login to api with facebook access token
            var LoginModel = Backbone.Model.extend({
            url: "/login",
             defaults: {

                  user: accessToken,
                  role: "abc"
                }
            });

            var loginModel = new LoginModel();
            loginModel.save(loginModel, {
              success: function(data){
                // if success, get api access token
                if (!data.get("error")){
                  console.log("Api access token: " + data.get("token"));
                  $.cookie("oauth_token", data.get("token"), { path: '/', expires: data.get("expire") });

                  // update user control
                  require(['views/user/userControl', 'views/header/header'], function (UserControlView, HeaderView) {
                    var userControlView = Vm.create(HeaderView, 'UserControlView', UserControlView);
                    userControlView.render();
                  });
                }
              }
            });
          }
      });

      // handle bad request
      $( document ).ajaxError(function( event, jqxhr, settings, exception ) {
          if ( jqxhr.status== 401 ) {
            console.log("Unauthorized access to " + settings.url);

            // get role
          //  var role = JSON.parse(jqxhr.responseText).Message;
         //   console.log("Role needed: " + role);

            // check fb login status
            var accessToken;
            FB.getLoginStatus(function(response){
              // if authorized -> get facebook accesstoken
              if (response.status === "connected"){
                var uid = response.authResponse.userID;
                accessToken = response.authResponse.accessToken;
                console.log("Facebook access token: " + accessToken);

                // login to api with facebook access token
                var LoginModel = Backbone.Model.extend({
                  url: "/login",
                  defaults: {
                  user: accessToken,
                  role: "abc"
                }
                  
                });

                var loginModel = new LoginModel();
                loginModel.save(loginModel, {
                  success: function(data){
                    // if success, get api access token
                    if (!data.get("error")){
                      console.log("Api access token: " + data.get("token"));
                      $.cookie("oauth_token", data.get("token"), { path: '/', expires: data.get("expire") });
                    }
                  }
                });
              }
              // if not, inform user to authorize app
              else{
              //    $("#fb-login-button").trigger("click");
                FB.login(function(response) { 
                  // if user accept, get access token
                  if (response.authResponse)
                    accessToken = response.authResponse.accessToken;
                  // if not
                 // else return;
                }, 
                {
                  scope:'read_stream,publish_stream,email'
                });
              }

              console.log(accessToken);
                
            });
          }

          if ( jqxhr.status== 403 ) {
            BootstrapDialog.show({
            title: "Lỗi",
            message: "Bạn không có đủ quyền",
            buttons: [{
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
              }]
            });
          }

      });

    },
    render: function () {
      var that = this;
      $(this.el).html(layoutTemplate);
      require(['views/header/header'], function (HeaderMenuView) {
        var headerMenuView = Vm.create(that, 'HeaderMenuView', HeaderMenuView);
        headerMenuView.render();
      });
    /*  require(['views/footer/footer'], function (FooterView) {
        // Pass the appView down into the footer so we can render the visualisation
        var footerView = Vm.create(that, 'FooterView', FooterView, {appView: that});
        footerView.render();
      });*/
    }
  });
  return AppView;
});
