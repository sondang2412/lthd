define([
  'views/app',
  'jquery',
  'lodash',
  'backbone',
  'text!templates/leftbar/leftbar.html',
  'views/home/home',
  'vm'
], function(appView, $, _, Backbone, leftbarTemplate, homepage, Vm){
  var DashboardPage = Backbone.View.extend({
    el: '#left-bar',
    render: function (forPoster) {
      var that = this;
      var DistrictModel = Backbone.Collection.extend({
        url: "district"
      });
      var districtList = new DistrictModel();
      districtList.fetch({
        success: function(districts){
        var template = _.template(leftbarTemplate, {districts: districts.models});
        that.$el.html(template);

        $(".district-select").click(function(){
            var homeView = Vm.create(appView, 'HomePage', homepage);
            homeView.render(1, $(this).attr('id'), forPoster);
            return false;
          });
        }
      });

    }
  });
  return DashboardPage;
});
