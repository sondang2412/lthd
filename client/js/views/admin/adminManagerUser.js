define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/admin/adminManagerUser.html',
  'bootstrapDialog'
], function($, _, Backbone, adminManagerUserTemplate){
  var userModel = Backbone.Model.extend({
      defaults: {
            id: 0,
            username: "Not specified",
            password: "Not specified",
            fullName: "Not specified",
            roleId : 0
        },
        parse : function(response) {
          return response
        },
        initialize: function(){
        }

  });
  var usersColection =  Backbone.Collection.extend({
    model: userModel,

    url: 'AdminManagerUser',
    initialize: function(){
      
    },
    parse: function(data){
      return data;
    }
    });
      

  var users = new usersColection();
  var HeaderMenuView = Backbone.View.extend({

    el: '#page-content',
    template: _.template(adminManagerUserTemplate),
    
    initialize: function () {
    },
     events: {
      "click .delete-btn": "confirmDelete",
    },
    confirmDelete : function(e){
      var dialogGlobalRef;

      BootstrapDialog.show({
          message: "Bạn có chắc không?",
          buttons: [{
              label: 'Yes',
              cssClass: 'btn-primary',
              autospin: true,
              action: function(dialogRef) {
                  dialogRef.enableButtons(false);
                  dialogRef.setClosable(false);
                  dialogGlobalRef = dialogRef;
                  doDelete();
              }
          }, {
              label: 'No',
              action: function(dialogRef) {
                  dialogRef.close();
              }
          }]
      });  // end confirm dialog show

      function doDelete(){
        var model = new userModel();
        model.id = parseInt($(e.currentTarget).attr('id'));
        model.url = "AdminManagerUser";
        var idSelect = 'drop-'+ $(e.currentTarget).attr('id');
        alert($("#" + idSelect).val());
        
        modelSave = {id: parseInt($(e.currentTarget).attr('id')), roleId : $("#" + idSelect).val() };
        
        model.save(modelSave, {
          success: function (model) {
            dialogGlobalRef.close();
            BootstrapDialog.show({
            message: "Thành công",
            buttons: [{
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
              }]
            });
          },
          error: function(){
            dialogGlobalRef.close();
            BootstrapDialog.show({
            message: "Có lỗi xảy ra, vui lòng thử lại",
            buttons: [{
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
              }]
            });
          }
        });
      }
      
      
      //xoa model
      //e.destroy();
    },

    render: function () {

    var that = this;
        users.fetch({
           success: function(){
            console.log(users);
             $(that.el).html(that.template({
              collection: users.models
            }));
        }
        });
     
    }
  })

  return HeaderMenuView;
});