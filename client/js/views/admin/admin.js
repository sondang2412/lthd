define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/huumy/huumy.html'
], function($, _, Backbone, huumyTemplate){
  var HeaderMenuView = Backbone.View.extend({
    el: '#page-content',
    initialize: function () {
    },
    render: function () {

      window.Course = Backbone.Model.extend({
        default: {},
        urlRoot: 'Course/'
      });
      $(this.el).html(huumyTemplate);
    }
  })

  return HeaderMenuView;
});
