define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/admin/adminArticle.html',
  'bootstrapDialog',
  'bootstrapPaginator'
], function($, _, Backbone, AdminArticleTemplate){
  var HeaderMenuView = Backbone.View.extend({
    el: '#page-content',
    initialize: function () {
    },
    render: function (curPage) {
      if (curPage == null)
        curPage = 1;

      var HouseModel = Backbone.Model.extend({
        url: "AdminArticle"
      });
      var houseModel = new HouseModel();
      var that = this;
      houseModel.fetch({
        data: $.param({CurPage: curPage}),
        success: function(houses){
          var template = _.template(AdminArticleTemplate, {houses: houses});
          $(that.el).html(template);

          var paginationOptions = {
            currentPage: curPage,
            totalPages: houses.get("TotalPage"),
            pageUrl: function(type, page, current) {
              return page;
            }
          };

          $('#pagination').bootstrapPaginator(paginationOptions);
          $('.pagination-item').click(function(){
            that.render($(this).attr('href'));
            return false;
          });
        }
      });
    },
    events:{
      "click .do-action-link": "confirm",
      "click .view-detail" : "viewDetail"
    },
    confirm: function(ev){
      ev.preventDefault();
      var dialogGlobalRef;
      var message ="";
      var action = $(ev.currentTarget).attr("data-action");
      if (action == "delete"){
        message = "Bạn có chắc muốn xóa bài này?"
      }
      else if (action == "approve"){
        message = "Bạn có chắc muốn duyệt bài này?"
      }

      BootstrapDialog.show({
          message: message,
          buttons: [{
              label: 'Yes',
              cssClass: 'btn-primary',
              autospin: true,
              action: function(dialogRef) {
                  dialogRef.enableButtons(false);
                  dialogRef.setClosable(false);
                  dialogGlobalRef = dialogRef;
                  doAction();
              }
          }, {
              label: 'No',
              action: function(dialogRef) {
                  dialogRef.close();
              }
          }]
      });  // end confirm dialog show

      function doAction(){
        var id = $(ev.currentTarget).parent("td").find("input[name=id]").val();
        var AdminArticleModel = Backbone.Model.extend({
          url: "AdminArticle"
        });
        var adminArticleModel = new AdminArticleModel({id: id, action:action});
        adminArticleModel.save(null, {
          success: function(){
            dialogGlobalRef.close();
            BootstrapDialog.show({
            message: "Thành công",
            buttons: [{
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
              }]
            });
          },
          error: function(){
            dialogGlobalRef.close();
            BootstrapDialog.show({
            message: "Có lỗi xảy ra, vui lòng thử lại",
            buttons: [{
                label: 'Close',
                action: function(dialogItself){
                    dialogItself.close();
                }
              }]
            });
          },
          complete: function(){
            dialogGlobalRef.close();
            require(['views/admin/adminArticle', 'vm', 'views/app'], function (AdminArticleView, Vm, appView) {
              var adminArticleView = Vm.create(appView, 'AdminArticleView', AdminArticleView);
              adminArticleView.render();
            })
          }
        })
      }

    }, // end confirm remove

    viewDetail: function(ev){
      ev.preventDefault();
      var id = $(ev.currentTarget).attr("data-value");
      require(['views/detail/detail', 'vm', 'views/app'], function (DetailArticleView, Vm, appView) {
        var detailArticleView = Vm.create(appView, 'DetailArticleView', DetailArticleView, {id: id});
        detailArticleView.render(id);
      })
    }
  })

  return HeaderMenuView;
});
