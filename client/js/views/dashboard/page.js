define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/dashboard/page.html',
  'facebook'
], function($, _, Backbone, dashboardPageTemplate){
  var DashboardPage = Backbone.View.extend({
    el: '.page',
    render: function () {
      // init facebook
      FB.init({
        appId : "238667939622562",
        status     : true, // check login status
        cookie     : true, // enable cookies to allow the server to access the session
        xfbml      : true  // parse XFBML
      });
      $(this.el).html(dashboardPageTemplate);
    }
  });
  return DashboardPage;
});
