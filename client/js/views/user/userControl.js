define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/user/userControl.html',
  'facebook',
  'jqueryCookie'
], function($, _, Backbone, userControlTemplate){
  var DashboardPage = Backbone.View.extend({
    el: '#user-control',
    render: function () {
      var LoginModel = Backbone.Model.extend({
        url: "Login"
      });
      var loginModel = new LoginModel();
      var that = this;
      loginModel.fetch({
        success: function(status){
          if (status.get("logged") == true){
            var UserModel = Backbone.Model.extend({ url: "User" });
            var userModel = new UserModel();
            userModel.fetch({
              success: function(user){
                var template = _.template(userControlTemplate, {user: user});
                that.$el.html(template);
              }
            });
          }
          else{
            that.$el.html("");
          }
        }
      });
    },
    events:{
      "click #edit-profile-link" : "goEditProfile",
      "click #logout-link" : "logout"
    },
    goEditProfile: function(){
      require(['views/profile/edit', 'vm', 'views/app'], function (EditProfileView, Vm, appView) {
        var editProfileView = Vm.create(appView, 'EditProfileView', EditProfileView);
        editProfileView.render();
      });
      return false;
    },
    logout: function(){
      var LogoutModel = Backbone.Model.extend({url: "Logout"});
      var logoutModel = new LogoutModel();
      logoutModel.save(null, {
        success: function(){
          // logout FB
          FB.logout();

          // delete cookie
          $.cookie("oauth_token", null);
          
          // update user control
          require(['views/user/userControl', 'vm', 'views/header/header'], function (UserControlView, Vm, HeaderView) {
            var userControlView = Vm.create(HeaderView, 'UserControlView', UserControlView);
            userControlView.render();
          });
        }
      })
    }
  });
  return DashboardPage;
});
