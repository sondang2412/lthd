define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/forbidden/forbidden.html',
], function($, _, Backbone, forbiddenPageTemplate){
  var ForbiddenPage = Backbone.View.extend({
    el: '.page',
    render: function () {
      alert("test");
      $(this.el).html(forbiddenPageTemplate);
    }
  });
  return ForbiddenPage;
});
