USE [RentHouseDB]
GO
ALTER TABLE [dbo].[UserProfile] DROP CONSTRAINT [FK__UserProfi__TypeA__4CA06362]
GO
ALTER TABLE [dbo].[UserProfile] DROP CONSTRAINT [FK__UserProfi__RoleI__1B0907CE]
GO
ALTER TABLE [dbo].[UserFollowArticle] DROP CONSTRAINT [FK_UserFollowArticle_UserProfile]
GO
ALTER TABLE [dbo].[UserFollowArticle] DROP CONSTRAINT [FK_UserFollowArticle_Article]
GO
ALTER TABLE [dbo].[Comment] DROP CONSTRAINT [FK__Comment__UserID__1FCDBCEB]
GO
ALTER TABLE [dbo].[Comment] DROP CONSTRAINT [FK__Comment__Article__1ED998B2]
GO
ALTER TABLE [dbo].[Article] DROP CONSTRAINT [FK_Article_District]
GO
ALTER TABLE [dbo].[Article] DROP CONSTRAINT [FK__Article__Type__4AB81AF0]
GO
ALTER TABLE [dbo].[Article] DROP CONSTRAINT [FK__Article__StateID__1BFD2C07]
GO
ALTER TABLE [dbo].[Article] DROP CONSTRAINT [FK__Article__Poster__1CF15040]
GO
/****** Object:  Index [UQ__UserProf__C9F28456023D5A04]    Script Date: 29/12/2013 2:44:26 PM ******/
ALTER TABLE [dbo].[UserProfile] DROP CONSTRAINT [UQ__UserProf__C9F28456023D5A04]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP TABLE [dbo].[UserRole]
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP TABLE [dbo].[UserProfile]
GO
/****** Object:  Table [dbo].[UserFollowArticle]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP TABLE [dbo].[UserFollowArticle]
GO
/****** Object:  Table [dbo].[Type]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP TABLE [dbo].[Type]
GO
/****** Object:  Table [dbo].[District]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP TABLE [dbo].[District]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP TABLE [dbo].[Comment]
GO
/****** Object:  Table [dbo].[AssistantStaff]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP TABLE [dbo].[AssistantStaff]
GO
/****** Object:  Table [dbo].[ArticleState]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP TABLE [dbo].[ArticleState]
GO
/****** Object:  Table [dbo].[Article]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP TABLE [dbo].[Article]
GO
/****** Object:  Table [dbo].[ADVERTISEMENT]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP TABLE [dbo].[ADVERTISEMENT]
GO
USE [master]
GO
/****** Object:  Database [RentHouseDB]    Script Date: 29/12/2013 2:44:26 PM ******/
DROP DATABASE [RentHouseDB]
GO
/****** Object:  Database [RentHouseDB]    Script Date: 29/12/2013 2:44:26 PM ******/
CREATE DATABASE [RentHouseDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RentHouseDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\RentHouseDB.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'RentHouseDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\RentHouseDB_log.ldf' , SIZE = 1040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [RentHouseDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RentHouseDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RentHouseDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RentHouseDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RentHouseDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RentHouseDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RentHouseDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [RentHouseDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RentHouseDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [RentHouseDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RentHouseDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RentHouseDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RentHouseDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RentHouseDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RentHouseDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RentHouseDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RentHouseDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RentHouseDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [RentHouseDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RentHouseDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RentHouseDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RentHouseDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RentHouseDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RentHouseDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RentHouseDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RentHouseDB] SET RECOVERY FULL 
GO
ALTER DATABASE [RentHouseDB] SET  MULTI_USER 
GO
ALTER DATABASE [RentHouseDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RentHouseDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RentHouseDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RentHouseDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'RentHouseDB', N'ON'
GO
USE [RentHouseDB]
GO
/****** Object:  Table [dbo].[ADVERTISEMENT]    Script Date: 29/12/2013 2:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ADVERTISEMENT](
	[AdvertisementID] [int] IDENTITY(1,1) NOT NULL,
	[Images] [varchar](1000) NULL,
	[Content] [varchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[AdvertisementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Article]    Script Date: 29/12/2013 2:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Article](
	[ArticleID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[Content] [nvarchar](3000) NOT NULL,
	[Images] [varchar](3000) NULL,
	[ThumbnailImages] [varchar](3000) NULL,
	[Address] [nvarchar](255) NULL,
	[District] [varchar](5) NULL,
	[Area] [float] NULL,
	[StateID] [int] NULL,
	[Poster] [int] NULL,
	[PostDate] [datetime] NULL,
	[Follower] [int] NULL,
	[Type] [int] NULL,
	[Longtitude] [float] NULL,
	[Latitude] [float] NULL,
	[PricePerDay] [float] NULL,
	[PricePerMonth] [float] NULL,
	[PricePerYear] [float] NULL,
 CONSTRAINT [PK__Article__9C6270C809DE7BCC] PRIMARY KEY CLUSTERED 
(
	[ArticleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ArticleState]    Script Date: 29/12/2013 2:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArticleState](
	[StateID] [int] IDENTITY(1,1) NOT NULL,
	[StateName] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[StateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssistantStaff]    Script Date: 29/12/2013 2:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssistantStaff](
	[AssistantStaffID] [int] IDENTITY(1,1) NOT NULL,
	[NameAssistant] [nvarchar](50) NULL,
	[YahooID] [varchar](50) NULL,
	[SkypeID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[AssistantStaffID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 29/12/2013 2:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[CommentID] [int] IDENTITY(1,1) NOT NULL,
	[ArticleID] [int] NULL,
	[UserID] [int] NULL,
	[Content] [nvarchar](3000) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[District]    Script Date: 29/12/2013 2:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[District](
	[DistrictID] [varchar](5) NOT NULL,
	[DistrictName] [nvarchar](20) NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[DistrictID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Type]    Script Date: 29/12/2013 2:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type](
	[TypeId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserFollowArticle]    Script Date: 29/12/2013 2:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserFollowArticle](
	[UserFollowArticleID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[ArticleID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserFollowArticleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 29/12/2013 2:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](255) NOT NULL,
	[Password] [varchar](255) NULL,
	[Email] [varchar](255) NOT NULL,
	[PhoneNumber] [varchar](15) NULL,
	[Address] [nvarchar](100) NULL,
	[TypeAccount] [int] NULL,
	[RoleID] [int] NULL,
	[FullName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK__UserProf__1788CCAC7F60ED59] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 29/12/2013 2:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__UserProf__C9F28456023D5A04]    Script Date: 29/12/2013 2:44:27 PM ******/
ALTER TABLE [dbo].[UserProfile] ADD  CONSTRAINT [UQ__UserProf__C9F28456023D5A04] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Article]  WITH CHECK ADD  CONSTRAINT [FK__Article__Poster__1CF15040] FOREIGN KEY([Poster])
REFERENCES [dbo].[UserProfile] ([UserID])
GO
ALTER TABLE [dbo].[Article] CHECK CONSTRAINT [FK__Article__Poster__1CF15040]
GO
ALTER TABLE [dbo].[Article]  WITH CHECK ADD  CONSTRAINT [FK__Article__StateID__1BFD2C07] FOREIGN KEY([StateID])
REFERENCES [dbo].[ArticleState] ([StateID])
GO
ALTER TABLE [dbo].[Article] CHECK CONSTRAINT [FK__Article__StateID__1BFD2C07]
GO
ALTER TABLE [dbo].[Article]  WITH CHECK ADD  CONSTRAINT [FK__Article__Type__4AB81AF0] FOREIGN KEY([Type])
REFERENCES [dbo].[Type] ([TypeId])
GO
ALTER TABLE [dbo].[Article] CHECK CONSTRAINT [FK__Article__Type__4AB81AF0]
GO
ALTER TABLE [dbo].[Article]  WITH CHECK ADD  CONSTRAINT [FK_Article_District] FOREIGN KEY([District])
REFERENCES [dbo].[District] ([DistrictID])
GO
ALTER TABLE [dbo].[Article] CHECK CONSTRAINT [FK_Article_District]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK__Comment__Article__1ED998B2] FOREIGN KEY([ArticleID])
REFERENCES [dbo].[Article] ([ArticleID])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK__Comment__Article__1ED998B2]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK__Comment__UserID__1FCDBCEB] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserProfile] ([UserID])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK__Comment__UserID__1FCDBCEB]
GO
ALTER TABLE [dbo].[UserFollowArticle]  WITH CHECK ADD  CONSTRAINT [FK_UserFollowArticle_Article] FOREIGN KEY([ArticleID])
REFERENCES [dbo].[Article] ([ArticleID])
GO
ALTER TABLE [dbo].[UserFollowArticle] CHECK CONSTRAINT [FK_UserFollowArticle_Article]
GO
ALTER TABLE [dbo].[UserFollowArticle]  WITH CHECK ADD  CONSTRAINT [FK_UserFollowArticle_UserProfile] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserProfile] ([UserID])
GO
ALTER TABLE [dbo].[UserFollowArticle] CHECK CONSTRAINT [FK_UserFollowArticle_UserProfile]
GO
ALTER TABLE [dbo].[UserProfile]  WITH CHECK ADD  CONSTRAINT [FK__UserProfi__RoleI__1B0907CE] FOREIGN KEY([RoleID])
REFERENCES [dbo].[UserRole] ([RoleID])
GO
ALTER TABLE [dbo].[UserProfile] CHECK CONSTRAINT [FK__UserProfi__RoleI__1B0907CE]
GO
ALTER TABLE [dbo].[UserProfile]  WITH CHECK ADD  CONSTRAINT [FK__UserProfi__TypeA__4CA06362] FOREIGN KEY([TypeAccount])
REFERENCES [dbo].[Type] ([TypeId])
GO
ALTER TABLE [dbo].[UserProfile] CHECK CONSTRAINT [FK__UserProfi__TypeA__4CA06362]
GO
USE [master]
GO
ALTER DATABASE [RentHouseDB] SET  READ_WRITE 
GO
