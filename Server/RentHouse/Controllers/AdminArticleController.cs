﻿using RentHouse.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RentHouse.Models;
using System.Web.Http.Cors;

namespace RentHouse.Controllers
{
    public class AdminArticleModel
    {
        public int id { get; set; }
        public string action { get; set; }
    }

    [CustomAuthorize(Roles = "Admin")]
    [NoCache]
    [EnableCors(origins: "http://localhost:1234, http://nhatro.parseapp.com", headers: "*", methods: "*")]
    public class AdminArticleController : ApiController
    {

        public object Get(int CurPage = 1)
        {
            RentHouseDBEntities db = new RentHouseDBEntities();
            SearchHouseModel model = new SearchHouseModel();

            List<Article> articleList = db.Articles.ToList();
            model.TotalPage = articleList.Count / 20;
            if (articleList.Count % 20 != 0)
                model.TotalPage++;
            articleList = articleList.Skip((CurPage - 1) * 20).Take(20).ToList();

            model.Result = new List<HouseModel>();
            foreach (Article a in articleList)
            {
                HouseModel hm = new HouseModel();
                hm.Id = a.ArticleID;
                hm.Title = a.Title;
                ArticleState state = db.ArticleStates.Find(a.StateID);
                if (state != null)
                {
                    hm.StateId = state.StateID;
                    hm.State = state.StateName;
                }

                model.Result.Add(hm);
            }

            return model;
        }

        public object Put([FromBody]AdminArticleModel value)
        {
            RentHouseDBEntities db = new RentHouseDBEntities();
            try
            {
                Article a = db.Articles.Find(value.id);
                if (a != null)
                {
                    if (value.action.Equals("delete"))
                    {
                        db.Entry(a).State = System.Data.EntityState.Deleted;
                        db.SaveChanges();
                    }
                    else if (value.action.Equals("approve"))
                    {
                        a.StateID = 2;
                        db.Entry(a).State = System.Data.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}
