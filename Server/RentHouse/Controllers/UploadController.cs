﻿using Google.GData.Client;
using Google.GData.Photos;
using RentHouse.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace RentHouse.Controllers
{
    [NoCache]
    [EnableCors(origins: "http://localhost:1234, http://nhatro.parseapp.com", headers: "*", methods: "*")]
    [CustomAuthorize(Roles="User,Admin")]
    public class UploadController : ApiController
    {

        [CustomAuthorize(Roles = "User,Admin")]
        public async Task<object> Post()
        {
            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.UnsupportedMediaType));
            }

            //login to picasa
            GDataGAuthRequestFactory authFactory = new GDataGAuthRequestFactory("lh2", "Picasa");
            authFactory.AccountType = "GOOGLE";
            PicasaService picasaService = new PicasaService("Picasa");
            picasaService.RequestFactory = authFactory;
            string username = "tntnhomh@gmail.com";
            string password = "nhomh123456";
            picasaService.setUserCredentials(username, password);
            string AuthToken = picasaService.QueryClientLoginToken();
            string albumID = "default";
            Uri postUri = new Uri(PicasaQuery.CreatePicasaUri(username, albumID));
            picasaService.SetAuthenticationToken(AuthToken);

            // upload image to picasa
            List<object> resp = new List<object>();
            var provider = new MultipartMemoryStreamProvider();
            try
            {
                try
                {
                    await Request.Content.ReadAsMultipartAsync(provider);
                }
                catch
                {
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
                }
                foreach (var item in provider.Contents)
                {
                    if (item.Headers.ContentDisposition.FileName != null)
                    {
                        Stream stream = item.ReadAsStreamAsync().Result;
                        PicasaEntry entry = (PicasaEntry)picasaService.Insert(postUri, stream,
                            "image/jpeg", item.Headers.ContentDisposition.FileName);
                        resp.Add(entry.Media.Thumbnails[2].Url);
                    }
                }
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.ToString());
            }

            return resp;
        }
    }
}
