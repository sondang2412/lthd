﻿using HtmlAgilityPack;
using RentHouse.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using RentHouse.Models;
using System.Globalization;
using System.Net.Http;
using System.Web.Http.Cors;

namespace RentHouse.Controllers
{
    [CustomAuthorize(Roles="Admin")]
    [NoCache]
    [EnableCors(origins: "http://localhost:1234, http://nhatro.parseapp.com", headers: "*", methods: "*")]
    public class ExtractInfoController : ApiController
    {
        private UserProfile poster = null;
        RentHouseDBEntities db;
        private void Extract(string url)
        {
            HtmlWeb hw = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = hw.Load(url);
          //  HtmlNodeCollection newitems = doc.DocumentNode.SelectNodes("//*[@id='colcenter']/div/div/div/div/ul/li");
            HtmlNodeCollection newitems = doc.DocumentNode.SelectNodes("//*[@id='colcenter']//li[@class='clearfix article']");
            if (newitems == null)
                return;

            foreach (var item in newitems)
            {
                try
                {
                    Article a = new Article();
                    string district = item.SelectSingleNode("div[3]/span").InnerText;
                    if (db.Districts.Where(m => m.DistrictName == district).Count() > 0)
                    {
                        a.District1 = db.Districts.Where(m => m.DistrictName == district).FirstOrDefault();
                    }
                    // url deltail
                    a.ThumbnailImages = item.SelectSingleNode("div[2]/a").GetAttributeValue("href", "");
                    // if not parsed => parse
                    if (db.Articles.Where(m => m.ThumbnailImages == a.ThumbnailImages).Count() == 0)
                    {
                        HtmlAgilityPack.HtmlDocument docDetail = hw.Load(a.ThumbnailImages);
                        HtmlNodeCollection itemDetail = docDetail.DocumentNode.SelectNodes("//*[@id='dnoidung_content']");
                        a.Title = itemDetail[0].SelectSingleNode("div[1]/text()").InnerText;
                        string time = itemDetail[0].SelectSingleNode("div[2]/span[4]").InnerText;
                        a.PostDate = DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        a.Address = itemDetail[0].SelectSingleNode("div[3]/span[2]").InnerText;
                        //string phonenumber = itemDetail[0].SelectSingleNode("div[5]/div[1]/span").InnerText;
                        string price = itemDetail[0].SelectSingleNode("div[5]/div[2]/span").InnerText;
                        if (!price.ToLower().Contains("vn"))
                            price = "0";
                        price = price.Replace(".", "").Replace("vnđ", "").Trim();
                        a.PricePerMonth = double.Parse(price);
                        string area = itemDetail[0].SelectSingleNode("div[5]/div[3]/span").InnerText;
                        area = area.Replace("m2", "");
                        a.Area = double.Parse(area);
                        a.Content = itemDetail[0].SelectSingleNode("div[6]").InnerText;

                        //List image
                        List<String> imgs = new List<String>();
                        string linkImg = itemDetail[0].SelectSingleNode("div[6]/center").InnerHtml;
                        if (linkImg.IndexOf("img src") > 0)
                        {
                            string[] words = linkImg.Split('"');
                            for (int i = 0; i < words.Length; i++)
                                if (words[i].Length > 60)
                                    imgs.Add(words[i]);
                        }
                        foreach (string img in imgs)
                        {
                            a.Images += img + ";";
                        }

                        a.StateID = 2;
                        //Save data here...
                        if (poster != null)
                            a.Poster = poster.UserID;
                        db.Articles.Add(a);
                        db.SaveChanges();
                    }
                }
                catch
                {
                    continue;
                }
            }
        }
        // GET: api/values

        public HttpResponseMessage Post(string oauth_token="")
        {
            db = new RentHouseDBEntities();
            try
            {
                poster = db.UserProfiles.Where(m => m.Password == oauth_token).Single();
         /*       Extract(@"http://kenhnhatro.com");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-1-ho-chi-minh-24-1-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-2-ho-chi-minh-24-2-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-3-ho-chi-minh-24-3-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-4-ho-chi-minh-24-4-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-5-ho-chi-minh-24-5-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-6-ho-chi-minh-24-6-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-7-ho-chi-minh-24-7-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-8-ho-chi-minh-24-8-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-9-ho-chi-minh-24-9-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-10-ho-chi-minh-24-10-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-11-ho-chi-minh-24-11-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-12-ho-chi-minh-24-12-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-phu-nhuan-ho-chi-minh-24-13-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-binh-thanh-ho-chi-minh-24-14-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-tan-binh-ho-chi-minh-24-15-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-tan-phu-ho-chi-minh-24-16-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-go-vap-ho-chi-minh-24-17-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-thu-duc-ho-chi-minh-24-18-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-quan-binh-tan-ho-chi-minh-24-19-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-huyen-binh-chanh-ho-chi-minh-24-20-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-huyen-cu-chi-ho-chi-minh-24-21-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-huyen-nha-be-ho-chi-minh-24-22-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-huyen-can-gio-ho-chi-minh-24-23-0-0-0.html");
                Extract(@"http://kenhnhatro.com/cho-thue-phong-tro-nha-tro-huyen-hoc-mon-ho-chi-minh-24-24-0-0-0.html");*/
                Extract(@"http://kenhnhatro.com");
            }
            catch(Exception ex)
            {
                //return new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);
            }

            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
        }

    }
}