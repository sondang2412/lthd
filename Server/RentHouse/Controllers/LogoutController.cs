﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RentHouse.Models;
using RentHouse.Attributes;
using System.Web.Http.Cors;

namespace RentHouse.Controllers
{
    [NoCache]
    [EnableCors(origins: "http://localhost:1234, http://nhatro.parseapp.com", headers: "*", methods: "*")]
    public class LogoutController : ApiController
    {
        public void Post(string oauth_token)
        {
            RentHouseDBEntities db = new RentHouseDBEntities();
            UserProfile up = db.UserProfiles.Where(m => m.Password == oauth_token).Single();
            up.Password = "";
            db.Entry(up).State = System.Data.EntityState.Modified;
            db.SaveChanges();
        }
    }
}
