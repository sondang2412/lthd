﻿using OAuth2.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Http.Cors;
using System.Net.Http.Headers;
using RentHouse.Attributes;
using RentHouse.Models;

namespace RentHouse.Controllers
{
    public class LoginModel
    {
        public string requestToken { get; set; }
        public string user { get; set; } // facebook access token
        public string role { get; set; }
        /*     public string password { get; set; }
             public bool? rememberMe { get; set; }
             public string returnUrl { get; set; }*/
    }

    [NoCache]
    [EnableCors("http://localhost:1234, http://nhatro.parseapp.com", "*", "*")]
    public class LoginController : ApiController
    {
        // check login status
        public object Get(string oauth_token="")
        {
            RentHouseDBEntities db = new RentHouseDBEntities();
            bool b = (db.UserProfiles.Where(m => m.Password == oauth_token).Count() > 0);
            return new { logged=b };
        }

        public object Post(LoginModel model)
        {
            RentHouseDBEntities db = new RentHouseDBEntities();
            model.requestToken = OAuthServiceBase.Instance.RequestToken().RequestToken;
            var accessResponse = OAuthServiceBase.Instance.AccessToken(model.requestToken, model.role, model.user);

            if (!accessResponse.Success)
            {
                OAuthServiceBase.Instance.UnauthorizeToken(model.requestToken);
                var requestResponse = OAuthServiceBase.Instance.RequestToken();

                return new
                {
                    error = "Invalid Credentials"
                };
            }
            else
            {
                /*       var cookie = new CookieHeaderValue(OAuthConstants.AuthorzationParam, accessResponse.AccessToken);
                       cookie.Expires = DateTimeOffset.Now.AddDays(1);
                       cookie.Domain = Request.RequestUri.Host;
                       cookie.Path = "/";*/
                return new { token = accessResponse.AccessToken, expire = accessResponse.Expires };
            }

            /*     ViewData["ReturnUrl"] = String.IsNullOrEmpty(returnUrl)
                                               ? "/"
                                               : returnUrl;

                 return View("Success", accessResponse);*/
        }
    }
}
