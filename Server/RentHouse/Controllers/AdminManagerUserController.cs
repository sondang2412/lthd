﻿using RentHouse.Attributes;
using RentHouse.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace RentHouse.Controllers
{
    public class UserModel
    {
        public int id { get; set; }
        public string  username { get; set; }
        public string  password { get; set; }
        public string fullName { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public int roleId { get; set; }
    }

     [EnableCors(origins: "http://localhost:1234, http://nhatro.parseapp.com", headers: "*", methods: "*")]
    [CustomAuthorize(Roles="Admin")]
    public class AdminManagerUserController : ApiController
    {
        public List<UserModel> Get() {
            RentHouseDBEntities RentHouseDb = new RentHouseDBEntities();
            List<UserProfile> userList = RentHouseDb.UserProfiles.ToList();
            List<UserModel> userModelList = new List<UserModel>();
            foreach (UserProfile item in userList)
            {
                UserModel user = new UserModel();
                user.id = item.UserID;
                user.username = item.UserName;
                user.password = item.Password;
                user.fullName = item.FullName;
                user.roleId = (int)item.RoleID;
                userModelList.Add(user);

            }
            return userModelList;
            
        }
        public void Delete(int id) {
            RentHouseDBEntities RentHouseDb = new RentHouseDBEntities();
            UserProfile user = RentHouseDb.UserProfiles.Where(u => u.UserID == id).SingleOrDefault();
            RentHouseDb.UserProfiles.Remove(user);
            RentHouseDb.SaveChanges();
        }

        public void Post()
        {

        }

        public void Put([FromBody]UserModel usermodel)
        {
            RentHouseDBEntities RentHouseDb = new RentHouseDBEntities();
             UserProfile user = RentHouseDb.UserProfiles.Where(u => u.UserID == usermodel.id).SingleOrDefault();
             user.RoleID = usermodel.roleId;
             RentHouseDb.Entry(user).State = System.Data.EntityState.Modified;
             RentHouseDb.SaveChanges();
        }
    }
   
}
