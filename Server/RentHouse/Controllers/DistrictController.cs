﻿using RentHouse.Attributes;
using RentHouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace RentHouse.Controllers
{
    public class DistrictModel
    {
        public string DistrictID { get; set; }
        public string DistrictName { get; set; }
    }
    [EnableCors("http://localhost:1234, http://nhatro.parseapp.com", "*", "*")]
    public class DistrictController : ApiController
    {

        public List<DistrictModel> Get()
        {
            RentHouseDBEntities db = new RentHouseDBEntities();
            List<District> districtList = db.Districts.ToList();
            List<DistrictModel> result = new List<DistrictModel>();
            foreach (District d in districtList)
            {
                DistrictModel dm = new DistrictModel();
                dm.DistrictID = d.DistrictID;
                dm.DistrictName = d.DistrictName;
                result.Add(dm);
            }
            return result;
        }
    }
}
