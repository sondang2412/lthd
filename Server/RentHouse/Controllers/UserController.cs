﻿using RentHouse.Attributes;
using RentHouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace RentHouse.Controllers
{
    [NoCache]
    [EnableCors("http://localhost:1234, http://nhatro.parseapp.com", "*", "*")]
    public class UserController : ApiController
    {
        [CustomAuthorize(Roles = "User,Admin")]
        public object Get(string oauth_token)
        {
            RentHouseDBEntities db = new RentHouseDBEntities();
            UserProfile up = db.UserProfiles.Where(m => m.Password == oauth_token).SingleOrDefault();
            if (up != null)
            {
                UserModel um = new UserModel();
                um.id = up.UserID;
                um.fullName = up.FullName;
                um.email = up.Email;
                um.phone = up.PhoneNumber;
                return um;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        [CustomAuthorize(Roles = "User,Admin")]
        public HttpResponseMessage Put([FromBody]UserModel value, string oauth_token)
        {
            RentHouseDBEntities db = new RentHouseDBEntities();
            UserProfile dbUser = db.UserProfiles.Find(value.id);
            if (dbUser.Password != oauth_token)
            {
                return new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
            dbUser.FullName = value.fullName;
            dbUser.Email = value.email;
            dbUser.PhoneNumber = value.phone;
            db.Entry(dbUser).State = System.Data.EntityState.Modified;
            db.SaveChanges();

            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
