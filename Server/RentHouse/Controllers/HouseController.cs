﻿using LinqKit;
using RentHouse.Attributes;
using RentHouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Cors;

namespace RentHouse.Controllers
{
    public class HouseModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public double Price { get; set; }
        public double Area { get; set; }
        public string ImgUrl { get; set; }
        public string DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string Address { get; set; }
        public double Longtitude { get; set; }
        public double Latitude { get; set; }
        public string PostDate { get; set; }
        public List<string> Imgs { get; set; }
        public string Poster { get; set; }
        public string State { get; set; }
        public int StateId { get; set; }
    }

    public class SearchHouseModel
    {
        public List<HouseModel> Result { get; set; }
        public int TotalPage { get; set; }
    }

    public class TestTestModel
    {
        public string Name { get; set; }
    }

    [NoCache]
    [EnableCors(origins: "http://localhost:1234, http://nhatro.parseapp.com", headers: "*", methods: "*")]
    public class HouseController : ApiController
    {

        private string SetImgSize(string input, int width)
        {
            Regex regex = new Regex("/s[^d]*/");
            return regex.Replace(input, "/s" + width.ToString() + "/");
        }

        public object Get(int Id, string oauth_token="")
        {
            try
            {
                RentHouseDBEntities db = new RentHouseDBEntities();
                Article a = db.Articles.Find(Id);
                UserProfile poster = db.UserProfiles.Find(a.Poster);
                // if article is in pending state => only admin and poster can view
                if (a.StateID == 1)
                {
                    UserProfile realPoster = db.UserProfiles.Where(m => m.Password == oauth_token).FirstOrDefault();
                    if (realPoster.RoleID != 1 && realPoster.UserID != poster.UserID)
                        return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                HouseModel hm = new HouseModel();
                hm.Id = a.ArticleID;
                hm.Title = a.Title;
                hm.Content = a.Content;

                hm.Poster = poster.FullName;
                DateTime postdate = a.PostDate ?? DateTime.Now;
                hm.PostDate = postdate.ToString("dd/MM/yyyy hh:mm:ss");
                hm.Address = a.Address;
                hm.DistrictName = a.District1.DistrictName;
                hm.Price = a.PricePerMonth ?? 0;
                hm.Area = a.Area ?? 0;
                hm.Imgs = new List<string>();
                hm.Longtitude = a.Longtitude ?? 0;
                hm.Latitude = a.Latitude ?? 0;
                string[] imgs;
                if (a.Images != null)
                {
                    imgs = a.Images.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string img in imgs)
                    {
                        hm.Imgs.Add(SetImgSize(img, 400));
                    }
                }
                return hm;
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        public object Get(bool ForPoster = false, string DistrictID = "",
            int ItemPerPage=5, int CurPage=1, int ImgSize=500, string oauth_token="")
        {
            RentHouseDBEntities db = new RentHouseDBEntities();
            try
            {
                SearchHouseModel model = new SearchHouseModel();

                var predicate = PredicateBuilder.True<Article>();
                if (!string.IsNullOrEmpty(DistrictID))
                    predicate = predicate.And(m => m.District1.DistrictID == DistrictID);

                if (ForPoster)
                {
                    try
                    {
                        UserProfile poster = db.UserProfiles.Where(m => m.Password == oauth_token).Single();
                        predicate = predicate.And(m => m.Poster == poster.UserID);
                    }
                    catch { return new HttpResponseMessage(HttpStatusCode.Unauthorized); }
                }

                predicate = predicate.And(m => m.StateID == 2);

                List<Article> articleList = db.Articles.Where(predicate.Expand()).ToList();
                model.TotalPage = articleList.Count / ItemPerPage;
                if (articleList.Count % ItemPerPage != 0)
                    model.TotalPage++;
                articleList = articleList.Skip((CurPage - 1) * ItemPerPage).Take(ItemPerPage).ToList();

                model.Result = new List<HouseModel>();
                foreach (Article a in articleList)
                {
                    HouseModel hm = new HouseModel();
                    hm.Id = a.ArticleID;
                    if (a.Images != null)
                        hm.ImgUrl = a.Images.Split(';').ElementAt(0);
                    else
                        hm.ImgUrl = @"https://lh6.googleusercontent.com/-Syl5BqHPB5A/UsaOx1G8l8I/AAAAAAAAAZA/XpUNTsKBARU/s500/800px-No_Image_Wide.svg.png";
                    hm.ImgUrl = SetImgSize(hm.ImgUrl, ImgSize);
                    hm.Title = a.Title;
                    hm.Content = a.Content;
                    hm.Address = a.Address;
                    hm.Price = a.PricePerMonth ?? 0;
                    hm.Area = a.Area ?? 0;
                    DateTime dt = a.PostDate ?? DateTime.Now;
                    hm.PostDate = dt.ToString("dd/MM/yyyy");
                    hm.DistrictName = a.District1.DistrictName;
                    model.Result.Add(hm);
                }

                return model;
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [CustomAuthorize(Roles = "User,Admin")]
        public void Post([FromBody]HouseModel value, string oauth_token)
        {
            RentHouseDBEntities db = new RentHouseDBEntities();
            Article article = db.Articles.Find(value.Id);
            bool create = false;
            if (article == null)
            {
                create = true;
                article = new Article();
            }
            article.Title = value.Title;
            article.Content = value.Content;
            article.PricePerMonth = value.Price;
            article.Area = value.Area;
            article.Images = value.ImgUrl;
            article.District1 = db.Districts.Where(m => m.DistrictID == value.DistrictId).Single();
            article.Address = value.Address;
            UserProfile poster = db.UserProfiles.Where(m => m.Password == oauth_token).Single();
            article.Poster = poster.UserID;
            article.Latitude = value.Latitude;
            article.Longtitude = value.Longtitude;
            article.PostDate = DateTime.Now;
            //if (db.ArticleStates.Where(m => m.StateName == "Pending").Count() == 0)
            //    db.ArticleStates.Add(new ArticleState() { StateName="Pending" });
            //article.StateID = db.ArticleStates.Where(m => m.StateName == "Pending").FirstOrDefault().StateID;
            article.StateID = 1;
            if (create)
                db.Articles.Add(article);
            else db.Entry(article).State = System.Data.EntityState.Modified;
            db.SaveChanges();
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}