﻿using System;
using System.Collections.Generic;
using System.Linq;
using OAuth2.Mvc;
using System.Web.Configuration;
using RentHouse.Models;

namespace RentHouse.OAuth
{
    public class DemoService : OAuthServiceBase
    {
        
        static DemoService()
        {
            Tokens = new List<DemoToken>();
            RequestTokens = new Dictionary<string, DateTime>();
        }

        public static List<DemoToken> Tokens { get; set; }

        public static Dictionary<String, DateTime> RequestTokens { get; set; }

        public override OAuthResponse RequestToken()
        {
            var token = Guid.NewGuid().ToString("N");
            var expire = DateTime.Now.AddMinutes(5);
            RequestTokens.Add(token, expire);

            return new OAuthResponse
                   {
                       Expires = (int)expire.Subtract(DateTime.Now).TotalSeconds,
                       RequestToken = token,
                       RequireSsl = false,
                       Success = true
                   };
        }
        //userName : fb acesstoken

        public override OAuthResponse AccessToken(string requestToken, string grantType, string fbtoken)
        {
            //kiem tra fbaccesstoken
            //get fbId

            RentHouseDBEntities RentHouseDb = new RentHouseDBEntities();
            Facebook.FacebookClient fbClient = new Facebook.FacebookClient();
            fbClient.AccessToken = fbtoken;
            fbClient.AppId = WebConfigurationManager.AppSettings["FBAppID"];
            fbClient.AppSecret = WebConfigurationManager.AppSettings["FBAppSecret"];
            dynamic info = fbClient.Get("/me");

            String userFbId = info.id;
            OAuthResponse OAuRes = CreateAccessToken(fbtoken);
            UserProfile user = RentHouseDb.UserProfiles.Where(u => u.UserName == userFbId).FirstOrDefault();
            if (user != null)
            {
                user.Password = OAuRes.AccessToken;
                RentHouseDb.Entry(user).State = System.Data.EntityState.Modified;
                RentHouseDb.SaveChanges();
            }else
            {
                user = new UserProfile();
                user.UserName = userFbId;
                user.Password = OAuRes.AccessToken;
                user.RoleID = 2;
                user.FullName = info.name;
                user.Email = info.email;
                RentHouseDb.UserProfiles.Add(user);
                RentHouseDb.SaveChanges();

            }
            //insert database
            return OAuRes;

          /*  return new OAuthResponse
                   {
                       Success = false
                   };*/
        }

        public override OAuthResponse RefreshToken(string refreshToken)
        {
            var token = Tokens.FirstOrDefault(t => t.RefreshToken == refreshToken);

            if (token == null)
                return new OAuthResponse
                       {
                           Error = "RefreshToken not found.",
                           Success = false
                       };

            if (token.IsRefreshExpired)
                return new OAuthResponse
                       {
                           Error = "RefreshToken expired.",
                           Success = false
                       };

            Tokens.Remove(token);
            return CreateAccessToken(token.Name);
        }

        private OAuthResponse CreateAccessToken(string name)
        {
            var token = new DemoToken(name);
            Tokens.Add(token);

            return new OAuthResponse
            {
                AccessToken = token.AccessToken,
                Expires = token.ExpireSeconds,
                RefreshToken = token.RefreshToken,
                RequireSsl = false,
                Success = true
            };
        }

        public override bool UnauthorizeToken(string accessToken)
        {
            var token = Tokens.FirstOrDefault(t => t.AccessToken == accessToken);
            if (token == null)
                return false;

            Tokens.Remove(token);
            return true;
        }
    }
}