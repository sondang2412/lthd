﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using Facebook;
using System.Security.Policy;
using System.Web.Http.Controllers;
using System.Net.Http.Headers;
using OAuth2.Mvc;
using RentHouse.Models;

namespace RentHouse.Attributes
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            RentHouseDBEntities RentHouseDb = new RentHouseDBEntities();
            string APIaccessToken = actionContext.Request.GetQueryNameValuePairs().Where(m => m.Key == "oauth_token").FirstOrDefault().Value;

            if (string.IsNullOrEmpty(APIaccessToken))
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(System.Net.HttpStatusCode.Unauthorized, new HttpError(Roles));
                return;
            }
            // api access token, check in database for role
            UserProfile user = RentHouseDb.UserProfiles.Where(u => u.Password == APIaccessToken).FirstOrDefault();
            if (user == null)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(System.Net.HttpStatusCode.Unauthorized, new HttpError(Roles));
                return;
            }
            //chec role user
            String [] roles = Roles.Split(',');
            if (roles == null)
            {
                return;
            }
            foreach (string role in roles)
            {
                if (String.Compare(user.UserRole.RoleName, role, true) == 0)
                {
                    //actionContext.Response = actionContext.Request.CreateErrorResponse(System.Net.HttpStatusCode.Forbidden, new HttpError(Roles));
                    return;
                }

            }

            actionContext.Response = actionContext.Request.CreateErrorResponse(System.Net.HttpStatusCode.Forbidden, new HttpError(Roles));
            return;
        }

        //protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        //{
        //  /*  string appID = "238667939622562";
        //    var fb = new FacebookClient();
        //    var loginUrl = fb.GetLoginUrl(new
        //    {
        //        client_id = appID,
        //        redirect_uri = "http://" + actionContext.Request.RequestUri.Authority + "/Home/FacebookLogin?returnUrl=" + actionContext.Request.RequestUri.ToString(),
        //        response_type = "code",
        //        scope = "read_stream, read_insights, offline_access"
        //    });

        //    var response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Redirect);
        //    response.Headers.Add("Location", loginUrl.ToString());
        //    actionContext.Response = response;*/
            
        //}
    }
}